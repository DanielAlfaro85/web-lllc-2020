const nomb = document.getElementById("name")
const ape = document.getElementById("apell")
const number = document.getElementById("num")
const form = document.getElementById("form")
const parrafo = document.getElementById("warnings")

var Acliente = [];

function guardar_localstorage() {
    let cliente = {
        nombre: nomb.value,
        apellido: ape.value,
        numero: number.value
    };

    Acliente.push(cliente);

    localStorage.setItem("cliente", JSON.stringify(Acliente));
}

function obtener() {
    let cliente = JSON.parse(localStorage.getItem("cliente"));
    console.log(cliente);
}

form.addEventListener("submit", e => {
    e.preventDefault()
    guardar_localstorage();
    obtener();
})